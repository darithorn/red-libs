Red/System [
    Title: "sdl2"
]

; placed outside sdl: context because `declare`
; doesn't support aliases inside a context
sdl-window!: alias struct! [
    tmp [integer!] ; this is just to get it to compile
]

sdl-renderer!: alias struct! [
    tmp [integer!]
]

sdl-texture!: alias struct! [
    tmp [integer!]
]

sdl-rwops!: alias struct! [
    tmp [integer!]
]

sdl-rect!: alias struct! [
    x [integer!] 
    y [integer!]
    w [integer!] 
    h [integer!]
]

sdl-surface!: alias struct! [
    format [integer!]
    w [integer!] 
    h [integer!]
    pitch [integer!]
    pixels [pointer! [integer!]]
    userdata [pointer! [integer!]]
    locked [integer!]
    lock-data [pointer! [integer!]]
    clip-rect [sdl-rect!]
    map [struct! [tmp [integer!]]]
    refcount [integer!]
]

sdl-glcontext!: alias struct! [
    tmp [integer!]
]

sdl-common-event!: alias struct! [
    type [integer!]
    timestamp [integer!]
]

sdl-window-event!: alias struct! [
    type [integer!]
    timestamp [integer!]
    window-id [integer!]
    event [byte!]
    data1 [integer!]
    data2 [integer!]
]

sdl-keysym!: alias struct! [
    scancode [integer!]
    keycode [integer!]
    mod [integer!]
    unused [integer!]
]

sdl-keyboard-event!: alias struct! [
    type [integer!]
    timestamp [integer!]
    window-id [integer!]
    state [byte!]
    repeat [byte!]
    padding2 [byte!]
    padding3 [byte!]
    keysym [sdl-keysym!]
]

sdl-textediting-event!: alias struct! [
    type [integer!]
    timestamp [integer!]
    window-id [integer!]
    text [c-string!]
    start [integer!]
    length [integer!]
]

sdl-textinput-event!: alias struct! [
    type [integer!]
    timestamp [integer!]
    window-id [integer!]
    text [c-string!]
]

sdl-mousemotion-event!: alias struct! [
    type [integer!]
    timestamp [integer!]
    window-id [integer!]
    which [integer!]
    state [integer!]
    x [integer!]
    y [integer!]
    xrel [integer!]
    yrel [integer!]
]

sdl-mousebutton-event!: alias struct! [
    type [integer!]
    timestamp [integer!]
    window-id [integer!]
    which [integer!]
    button [byte!]
    state [byte!]
    clicks [byte!]
    x [integer!]
    y [integer!]
]

sdl-mousewheel-event!: alias struct! [
    type [integer!]
    timestamp [integer!]
    window-id [integer!]
    which [integer!]
    x [integer!]
    y [integer!]
    direction [integer!]
]

sdl-event!: alias struct! [
    type [integer!]
    common [sdl-common-event!]
    window [sdl-window-event!]
    key [sdl-keyboard-event!]
    edit [sdl-textediting-event!]
    text [sdl-textinput-event!]
    motion [sdl-mousemotion-event!]
    button [sdl-mousebutton-event!]
    wheel [sdl-mousewheel-event!]
]

sdl: context [
    ; SDL.h literals
    init-timer: 00000001h
    init-audio: 00000010h
    init-video: 00000020h
    init-joystick: 00000200h
    init-haptic: 00001000h
    init-gamecontroller: 00002000h
    init-events: 00004000h
    init-noparachute: 00100000h
    init-everything: init-timer or init-audio or init-video or init-joystick
                    or init-haptic or init-gamecontroller or init-events or init-noparachute
    ; end SDL.h literals
    ; SDL_video.h literals
    window-shown: 00000004h
    window-opengl: 00000002h
    windowpos-undefined-mask: 1FFF0000h
    windowpos-undefined: windowpos-undefined-mask or 0 
    ;end SDL_video.h literals
    renderer-software: 00000001h
    renderer-accelerated: 00000002h

    ; SDL_events.h literals
    event: context [
        released: 0
        pressed: 1
        quit: 0100h
        window-event: 0200h
        key-down: 0300h
        key-up: 0301h
        mouse-motion: 0400h
        mouse-button-down: 0401h
        mouse-button-up: 0402h
        mouse-wheel: 0403h
    ]
    ; end SDL_events.h literals

    #import [
        "SDL2.dll" cdecl [
            ; SDL.h
            init: "SDL_Init" [
                flags [integer!]
                return: [integer!]
            ]
            quit: "SDL_Quit" []
            ; end SDL.h
            ; SDL_video.h
            create-window: "SDL_CreateWindow" [
                title [c-string!]
                x [integer!]
                y [integer!]
                w [integer!]
                h [integer!]
                flags [integer!]
                return: [sdl-window!]
            ]
            destroy-window: "SDL_DestroyWindow" [
                window [sdl-window!]
            ]
            ; end SDL_video.h
            ; SDL_events.h
            poll-event: "SDL_PollEvent" [
                event [sdl-event!]
                return: [integer!]
            ]
            ; end SDL_events.h
            create-renderer: "SDL_CreateRenderer" [
                window [sdl-window!]
                index [integer!]
                flags [integer!]
                return: [sdl-renderer!]
            ]
            destroy-renderer: "SDL_DestroyRenderer" [
                renderer [sdl-renderer!]
            ]
            render-clear: "SDL_RenderClear" [
                renderer [sdl-renderer!]
                return: [integer!]
            ]
            render-copy: "SDL_RenderCopy" [
                renderer [sdl-renderer!]
                texture [sdl-texture!]
                srcrect [sdl-rect!]
                dstrect [sdl-rect!]
                return: [integer!]
            ]
            render-present: "SDL_RenderPresent" [
                renderer [sdl-renderer!]
            ]
            ; SDL_Texture
            create-texture-from-surface: "SDL_CreateTextureFromSurface" [
                renderer [sdl-renderer!]
                surface [sdl-surface!]
                return: [sdl-texture!]
            ]
            destroy-texture: "SDL_DestroyTexture" [
                texture [sdl-texture!]
            ]
            ; end SDL_Texture
            rw-from-file: "SDL_RWFromFile" [
                file [c-string!]
                mode [c-string!]
                return: [sdl-rwops!]
            ]
            ; SDL_Surface
            load-bmp-rw: "SDL_LoadBMP_RW" [
                src [sdl-rwops!]
                freesrc [integer!]
                return: [sdl-surface!]
            ]
            free-surface: "SDL_FreeSurface" [
                surface [sdl-surface!]
            ]
            ; End SDL_Surface
        ]
    ]
    gl: context [
        #import [
            "SDL2.dll" cdecl [
                create-context: "SDL_GL_CreateContext" [
                    window [sdl-window!]
                    return: [sdl-glcontext!]
                ]
                swap-window: "SDL_GL_SwapWindow" [
                    window [sdl-window!]
                ]
                delete-context: "SDL_GL_DeleteContext" [
                    context [sdl-glcontext!]
                ]
                get-proc-address: "SDL_GL_GetProcAddress" [
                    proc [c-string!]
                    return: [pointer! [integer!]]
                ]
            ]
        ]
    ]

    load-bmp: func [file [c-string!] return: [sdl-surface!]][
        load-bmp-rw (rw-from-file file "rb") 1
    ]
]
